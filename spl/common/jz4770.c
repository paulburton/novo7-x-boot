#include <config.h>

/*
 * Init PLL.
 *
 * PLL output clock = EXTAL * NF / (NR * NO)
 *
 * NF = FD + 2, NR = RD + 2
 * NO = 1 (if OD = 0), NO = 2 (if OD = 1 or 2), NO = 4 (if OD = 3)
 */
#define DIV_1 0
#define DIV_2 1
#define DIV_3 2
#define DIV_4 3
#define DIV_6 4
#define DIV_8 5
#define DIV_12 6
#define DIV_16 7
#define DIV_24 8
#define DIV_32 9

#define DIV(a,b,c,d,e,f)					\
({								\
	unsigned int retval;					\
	retval = (DIV_##a << CPM_CPCCR_CDIV_BIT)   |		\
		 (DIV_##b << CPM_CPCCR_H0DIV_BIT)  |		\
		 (DIV_##c << CPM_CPCCR_PDIV_BIT)   |		\
		 (DIV_##d << CPM_CPCCR_C1DIV_BIT)  |		\
		 (DIV_##e << CPM_CPCCR_H2DIV_BIT)  |		\
		 (DIV_##f << CPM_CPCCR_H1DIV_BIT);		\
	retval;							\
})

void init_pll(void) 
{
	register unsigned int cfcr, plcr1, plcr2;

	/** divisors,
	 *  for jz4770 ,C:H0:P:C1:H2:H1.
	 *  DIV should be one of [1, 2, 3, 4, 6, 8, 12, 16, 24, 32]
	 */
	int pllout2;
//	unsigned int div = DIV(1,6,6,2,6,3);
	unsigned int div = DIV(1,4,8,2,4,4);
	cfcr = 	div;

	// write REG_DDRC_CTRL 8 times to clear ddr fifo
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;
	REG_DDRC_CTRL = 0;

	/* set CPM_CPCCR_MEM only for ddr1 or ddr2 */
#if (defined(CONFIG_SDRAM_DDR1) || defined(CONFIG_SDRAM_DDR2))
	cfcr |= CPM_CPCCR_MEM;
#else	/* mddr or sdram */
	cfcr &= ~CPM_CPCCR_MEM;
#endif
	cfcr |= CPM_CPCCR_CE;

	pllout2 = (cfcr & CPM_CPCCR_PCS) ? (CFG_CPU_SPEED / 2) : CFG_CPU_SPEED;

	plcr1 = CPCCR_M_N_OD;
	plcr1 |= (0x20 << CPM_CPPCR_PLLST_BIT)	/* PLL stable time */
		| CPM_CPPCR_PLLEN;             /* enable PLL */

	/*
	 * Init USB Host clock, pllout2 must be n*48MHz
	 * For JZ4760 UHC - River.
	 */
	REG_CPM_UHCCDR = pllout2 / 48000000 - 1;
	/* init PLL */
	REG_CPM_CPCCR = cfcr;
	REG_CPM_CPPCR = plcr1;

	/*wait for pll output stable ...*/
	while (!(REG_CPM_CPPCR & CPM_CPPCR_PLLS));

	/* set CPM_CPCCR_MEM only for ddr1 or ddr2 */
	plcr2 = CPCCR1_M_N_OD | CPM_CPPCR1_PLL1EN;

	/* init PLL_1 , source clock is extal clock */
	REG_CPM_CPPCR1 = plcr2;

	__cpm_enable_pll_change();

	/*wait for pll_1 output stable ...*/
	while (!(REG_CPM_CPPCR1 & CPM_CPPCR1_PLL1S));
	/*
	   serial_puts("REG_CPM_CPCCR = ");
	   serial_put_hex(REG_CPM_CPCCR);
	   serial_puts("REG_CPM_CPPCR = ");
	   serial_put_hex(REG_CPM_CPPCR);
	 */
}

