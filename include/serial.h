#ifndef _SERIAL_H
#define _SERIAL_H

/*
 * Serial common functions
 */

void serial_puts_info (const char *s);
void serial_puts_msg (const char *s);
void serial_puts (const char *s);
void serial_putc (const char c);
int init_serial (void);
void serial_put_hex(unsigned int  d);
#endif
