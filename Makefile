#########################################################################
#
# Makefile for XBoot.
#
# Copyright (C) 2005 - 2010  Ingenic Semiconductor Corp.
#
#########################################################################

SUBDIRS = spl boot

#########################################################################

TOPDIR	:= $(shell /bin/pwd)
export TOPDIR

obj :=
src :=
export obj src

CROSS_COMPILE = mips-linux-gnu-
export CROSS_COMPILE

#########################################################################

# load other configuration
ifeq ($(TOPDIR)/include/config.mk,$(wildcard $(TOPDIR)/include/config.mk))
include $(TOPDIR)/include/config.mk
endif

#########################################################################

ifeq ($(CONFIG_JZ4750),y)
CPU_TYPE = JZ4750
endif

ifeq ($(CONFIG_JZ4750L),y)
CPU_TYPE = JZ4750L
endif

ifeq ($(CONFIG_JZ4760),y)
CPU_TYPE = JZ4760
endif

ifeq ($(CONFIG_JZ4760B),y)
CPU_TYPE = JZ4760B
endif

ifeq ($(CONFIG_JZ4770),y)
CPU_TYPE = JZ4770
endif

export CPU_TYPE

#########################################################################
#########################################################################

ifeq ($(CONFIG_NAND_X_BOOT),y)
NAND_SPL = nand-spl.bin
X_BOOT_NAND = $(obj)x-boot-nand.bin
SPL_LOAD_ADDR = 0x80000000
BOOTTYPE = nand
endif

ifeq ($(CONFIG_SD_X_BOOT),y)
SD_SPL = sd-spl.bin
X_BOOT_SD = $(obj)x-boot-sd.bin
SPL_LOAD_ADDR = 0x80000000
BOOTTYPE = sd
endif

ifeq ($(CONFIG_MSC_X_BOOT),y)
MSC_SPL = msc-spl.bin
X_BOOT_MSC = $(obj)x-boot-msc.bin
SPL_LOAD_ADDR = 0x80000200
BOOTTYPE = msc
endif

X_BOOT_LOAD_ADDR = 0x80100000

export BOOTTYPE SPL_LOAD_ADDR X_BOOT_LOAD_ADDR

#########################################################################
#########################################################################

ifeq ($(CONFIG_JZ4760_PT701_8),y)
BOARDDIR = pt701_8
endif

export BOARDDIR

#########################################################################
#########################################################################

X_BOOT = x-boot.bin

MAKE := make -s
ALL = $(X_BOOT) $(X_BOOT_NAND) $(X_BOOT_SD) $(X_BOOT_MSC)

all:		$(ALL)

$(X_BOOT):
		$(MAKE) -C boot $@
		cp boot/x-boot.bin .

$(NAND_SPL):
		$(MAKE) -C spl $@

$(X_BOOT_NAND):	$(NAND_SPL) $(X_BOOT)
		$(MAKE) -C spl pad
		cat spl/nand-spl-pad.bin boot/x-boot.bin > x-boot-nand.bin

$(MSC_SPL):
		$(MAKE) -C spl $@

$(X_BOOT_MSC):	$(MSC_SPL) $(X_BOOT) MBR
		$(MAKE) -C spl pad
		cat spl/msc-spl-pad.bin boot/x-boot.bin > x-boot-msc.bin
		cat mbr.bin x-boot-msc.bin > mbr-xboot.bin

MBR:
		gcc spl/tools/mbr_creater/mbr_creater.c -o spl/tools/mbr_creater/mbr_creater
		spl/tools/mbr_creater/mbr_creater mbr.bin

$(LIBS):
		$(MAKE) -C $(dir $(subst $(obj),,$@))

#########################################################################

unconfig:
	@rm -f $(obj)include/config.h
	@rm -f $(obj)boot/mklogo.sh
	@echo "#!/bin/sh" > boot/mklogo.sh
	@chmod 777 boot/mklogo.sh
	@chmod 777 boot/_mklogo.sh
	@rm -rf ${PWD}/spl/common/jz_serial.c
	@rm -rf ${PWD}/spl/common/cpu.c
	@rm -rf ${PWD}/spl/common/debug.c
	@rm -rf ${PWD}/spl/common/common.c
	@rm -rf spl/tools/mbr_creater/mbr_creater
	@rm -rf spl/tools/mbr_creater/mbr.h

#########################################################################
#########################################################################

apus_nand_config:	unconfig
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "Compile NAND boot image for apus"
	@./mkconfig jz4750 apus
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4750 = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

apus_sd_config:		unconfig
	@echo "#define CONFIG_SD_X_BOOT" > include/config.h
	@echo "Compile SD boot image for apus"
	@./mkconfig jz4750 apus
	@echo "CONFIG_SD_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4750 = y" >> include/config.mk

aquila_nand_config:       unconfig
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "Compile NAND boot image for aquila"
	@./mkconfig jz4750 aquila
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4750 = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

altair_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_SAMSUNG_LMS350DF04 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_ALTAIR 1" >> include/config.h
	@echo "Compile NAND boot image for altair"
	@./mkconfig jz4760 altair
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_ALTAIR = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

altair_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_SAMSUNG_LMS350DF04 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_ALTAIR 1" >> include/config.h
	@echo "Compile NAND boot image for altair"
	@./mkconfig jz4760b altair
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_ALTAIR = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

crater_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_CRATER 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile NAND boot image for crater"
	@./mkconfig jz4760 crater
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_CRATER = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

crater_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_CRATER 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile NAND boot image for crater"
	@./mkconfig jz4760b crater
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_CRATER = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lepus_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_JZ4760_LEPUS 1" >> include/config.h
	@echo "Compile NAND boot image for lepus"
	@./mkconfig jz4760 lepus
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LEPUS = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468 " >> include/config.h
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c
lepus_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_AUO_A043FL01V2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_LEPUS 1" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "Compile MSC boot image for lepus"
	@./mkconfig jz4760 lepus msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LEPUS = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
#	@echo "./_mklogo.sh pt701_8.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c	

lepus_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_JZ4760_LEPUS 1" >> include/config.h
	@echo "Compile NAND boot image for lepus"
	@./mkconfig jz4760b lepus
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LEPUS = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468 " >> include/config.h
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lepus_jz4760b_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_LEPUS 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile MSC boot image for lepus"
	@./mkconfig jz4760b lepus msc
	@echo "CONFIG_MSC_X_BOOT = y" > $(obj)include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LEPUS = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> $(obj)include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lynx_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_LYNX 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile NAND boot image for lynx"
	@./mkconfig jz4760 lynx
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LYNX = y" >> include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lynx_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_LYNX 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile MSC boot image for lynx"
	@./mkconfig jz4760 lynx msc
	@echo "CONFIG_MSC_X_BOOT = y" > $(obj)include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LYNX = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> $(obj)include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lynx_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_LYNX 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile NAND boot image for lynx"
	@./mkconfig jz4760b lynx
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LYNX = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lynx_jz4760b_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_LYNX 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "#define CONFIG_XBOOT_POWERON_LONG_PRESSED" >> include/config.h
	@echo "Compile MSC boot image for lynx"
	@./mkconfig jz4760b lynx msc
	@echo "CONFIG_MSC_X_BOOT = y" > $(obj)include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LYNX = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> $(obj)include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

lynx_jz4760b13_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_ANDROID" >> $(obj)include/config.h
	@echo "#define CONFIG_JZ4760_LYNX 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_LYNX13 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "#define CONFIG_XBOOT_POWERON_LONG_PRESSED" >> include/config.h
	@echo "Compile MSC boot image for lynx"
	@./mkconfig jz4760b lynx msc
	@echo "CONFIG_MSC_X_BOOT = y" > $(obj)include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_LYNX = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> $(obj)include/config.mk
	@echo "./_mklogo.sh logo.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c	
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

z800_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
#	@echo "#define CONFIG_JZ4760_ANDROID_LCD_YUE_TFT_YL10922NT 1" >> include/config.h
#	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TRULY_TFT_ER61581 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_PROBE 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_Z800 1" >> include/config.h
	@echo "#define FAST_BOOT_SUPPORT" >> include/config.h
#	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for z800"
	@./mkconfig jz4760 z800
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_Z800 = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pt701_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_PT701 1" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for pt701"
	@./mkconfig jz4760 pt701
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PT701 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pt701_8_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define CONFIG_JZ4760_PT701_8 1" >> include/config.h
#	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for pt701 VM-tablet-8"
	@./mkconfig jz4760 pt701_8
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PT701_8 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh pt701_8.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pt701_8_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_PT701_8 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile MSC boot image for pt701 VM-tablet-8"
	@./mkconfig jz4760 pt701_8 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PT701_8 = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh pt701_8.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

#	@echo "TEXT_BASE = 0x80100200" > $(obj)board/lepus/config.tmp
#	@echo "CONFIG_CPU_TYPE = 4760" >> $(obj)include/config.mk
pt701_8_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define CONFIG_JZ4760_PT701_8 1" >> include/config.h
#	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for pt701 VM-tablet-8"
	@./mkconfig jz4760b pt701_8
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PT701_8 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh pt701_8.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pt701_8_jz4760b_msc_config:	unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_PT701_8 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile MSC boot image for pt701 VM-tablet-8"
	@./mkconfig jz4760b pt701_8 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PT701_8 = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh pt701_8.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_p2_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_P2 1" >> include/config.h
#	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for VM-tablet-p2"
	@./mkconfig jz4760 tablet_p2
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_P2 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_p2.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_p2_jz4760b_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_P2 1" >> include/config.h
	@echo "Compile NAND boot image for VM-tablet-p2"
	@./mkconfig jz4760b tablet_p2
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_P2 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_p2.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_p2_jz4760b_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_P2 1" >> include/config.h
	@echo "Compile msc boot image for VM-tablet-p2"
	@./mkconfig jz4760b tablet_p2 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_P2 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_p2.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_p2_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_P2 1" >> include/config.h
	@echo "Compile NAND boot image for VM-tablet-p2"
	@./mkconfig jz4760 tablet_p2 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_P2 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_p2.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_p27c_msc_config: tablet_p2_msc_config 
	@echo "./_mklogo.sh AMT.rle" > boot/mklogo.sh

tablet_p27ii_msc_config: tablet_p2_msc_config 
	@echo "./_mklogo.sh AMT.rle" > boot/mklogo.sh

tablet_8II_nand_config:       unconfig			
	@echo "#define CONFIG_NAND_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_8II 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile NAND boot image for tablet_8II VM-tablet-8II"
	@./mkconfig jz4760 tablet_8II
	@echo "CONFIG_NAND_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_8II = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_8II.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_8II_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_8II 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile MSC boot image for tablet_8II VM-tablet-8II"
	@./mkconfig jz4760 tablet_8II msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_8II = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_8II.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

tablet_8II_jz4760b_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_TABLET_8II 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 14260" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile MSC boot image for tablet_8II VM-tablet-8II"
	@./mkconfig jz4760b tablet_8II msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_JZ4760_TABLET_8II = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_8II.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pyxis_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_ANDROID_LCD_FOXCONN_PT035TN01 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_PYXIS 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 PYXIS BOARD"
	@./mkconfig jz4770 pyxis msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_PYXIS = y" >> include/config.mk
#	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

pisces_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_PISCES 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
#	@echo "#define CONFIG_SDRAM_DDR2 1" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 PISCES BOARD"
	@./mkconfig jz4770 pisces msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_PISCES = y" >> include/config.mk
#	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c


maple_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_ANDROID_LCD_KD080D3_40NB_A3 1" >> include/config.h
	@echo "#define CONFIG_LCD_SYNC_MODE 1" >> include/config.h
	@echo "#define DEFAULT_BACKLIGHT_LEVEL	20" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_XBOOT_POWERON_LONG_PRESSED" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_BYD_BM1024600 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_MAPLE 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
# 	MAPLE V1.1 disable DIC_NORMAL, V1.0 enable it
#	@echo "#define CONFIG_DDR2_DIC_NORMAL 1" >> include/config.h
# 	MAPLE V1.1 enable DRV_CK_CS_FULL V1.0 disable it
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
#	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
#	@echo "#define CONFIG_SDRAM_DDR2 1" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 MAPLE BOARD"
	@./mkconfig jz4770 maple msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_ACT8600 = y" >> include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_MAPLE = y" >> include/config.mk
#	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c
	
umido706_msc_config:       unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_ANDROID_LCD_KD080D3_40NB_A3 1" >> include/config.h
	@echo "#define CONFIG_LCD_SYNC_MODE 1" >> include/config.h
	@echo "#define DEFAULT_BACKLIGHT_LEVEL	20" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_XBOOT_POWERON_LONG_PRESSED" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_UMIDO706 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
#	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 UMIDO706 BOARD"
	@./mkconfig jz4770 umido706 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_ACT8600 = y" >> include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_UMIDO706 = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

npm701_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_NPM701 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 124568" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 NPM701 BOARD"
	@./mkconfig jz4770 npm701 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_NPM701 = y" >> include/config.mk
	@echo "CONFIG_ACT8600 = y" >> include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh novo7_1.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c
npm701_ab_msc_config: unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_NPM701_V_1_1 1" >> include/config.h
	@echo "#define CONFIG_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_NPM701 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 124568" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 NPM701 BOARD"
	@./mkconfig jz4770 npm701 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_NPM701 = y" >> include/config.mk
	@echo "CONFIG_ACT8600 = y" >> include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh novo7_1.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

px7_2r_jz4760b_msc_config:		unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_JZ4760_ANDROID_LCD_CPT_CLAA070MA21BW 1" >> include/config.h
	@echo "#define CONFIG_JZ4760B 1" >> include/config.h
	@echo "#define CONFIG_JZ4760_PX7_2R 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36580" >> include/config.h
	@echo "#define DEBUG" >> include/config.h
	@echo "Compile MSC boot image for px7_2r VM-px7-2r"
	@./mkconfig jz4760b px7_2r msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4760B = y" >> include/config.mk
	@echo "CONFIG_ACT8930 = y" >> include/config.mk
	@echo "CONFIG_JZ4760_PX7_2R = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh tablet_p2.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

amt_8II_nand_config:      tablet_8II_nand_config
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 51812" >> include/config.h
	@echo "./_mklogo.sh AMT_800x480.rle" >> boot/mklogo.sh

zorroo_msc_config:		unconfig
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
	@echo "#define CONFIG_ANDROID_LCD_SAMSUNG_LTN097XL01_A01 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_ZORROO 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
# 	ZORROO V1.1 disable DIC_NORMAL, V1.0 enable it
#	@echo "#define CONFIG_DDR2_DIC_NORMAL 1" >> include/config.h
# 	ZORROO V1.1 enable DRV_CK_CS_FULL V1.0 disable it
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
#	@echo "#define CONFIG_SDRAM_DDR2 1" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 ZORROO BOARD"
	@./mkconfig jz4770 zorroo msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_ZORROO = y" >> include/config.mk
#	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_1024_768.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

o1_msc_config:       unconfig			
	@echo "#define CONFIG_MSC_X_BOOT" > include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_KD50G2_40NM_A2 1" >> include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_TFT_AT070TN93 1" >> include/config.h
	@echo "#define CONFIG_ANDROID_LCD_KD080D3_40NB_A3 1" >> include/config.h
	@echo "#define CONFIG_LCD_SYNC_MODE 1" >> include/config.h
	@echo "#define DEFAULT_BACKLIGHT_LEVEL	20" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOW_BATTERY_DETECT" >> include/config.h
	@echo "#define CONFIG_XBOOT_POWERON_LONG_PRESSED" >> include/config.h
	@echo "#define I2C_BASE I2C1_BASE" >> include/config.h
#	@echo "#define CONFIG_ANDROID_LCD_BYD_BM1024600 1" >> include/config.h
	@echo "#define CONFIG_JZ4770 1" >> include/config.h
	@echo "#define CONFIG_JZ4770_O1 1" >> include/config.h
	@echo "#define CONFIG_DDR2_DIFFERENTIAL 1" >> include/config.h
# 	O1 V1.1 disable DIC_NORMAL, V1.0 enable it
#	@echo "#define CONFIG_DDR2_DIC_NORMAL 1" >> include/config.h
# 	O1 V1.1 enable DRV_CK_CS_FULL V1.0 disable it
	@echo "#define CONFIG_DDR2_DRV_CK_CS_FULL 1" >> include/config.h
	@echo "#define CONFIG_MSC_TYPE_SD" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE 1" >> include/config.h
	@echo "#define CONFIG_XBOOT_LOGO_FILE_LEN 36468" >> include/config.h
#	@echo "#define CONFIG_SDRAM_DDR2 1" >> include/config.h
	@echo "Compile MSC boot image for soc jz4770 O1 BOARD"
	@./mkconfig jz4770 o1 msc
	@echo "CONFIG_MSC_X_BOOT = y" > include/config.mk
	@echo "CONFIG_ACT8600 = y" >> include/config.mk
	@echo "CONFIG_JZ4770 = y" >> include/config.mk
	@echo "CONFIG_POWER_MANAGEMENT = y" >> include/config.mk
	@echo "CONFIG_JZ4770_O1 = y" >> include/config.mk
#	@echo "CONFIG_USE_MDDR = y" >> include/config.mk
	@echo "CONFIG_USE_DDR2 = y" >> include/config.mk
	@echo "./_mklogo.sh jz_800_480.rle" >> boot/mklogo.sh
	@ln -s ${PWD}/boot/common/jz_serial.c ${PWD}/spl/common/jz_serial.c
	@ln -s ${PWD}/boot/common/cpu.c ${PWD}/spl/common/cpu.c
	@ln -s ${PWD}/boot/common/debug.c ${PWD}/spl/common/debug.c
	@ln -s ${PWD}/boot/common/common.c ${PWD}/spl/common/common.c

release_config:
	echo "CONFIG_RELEASE = y" >> include/config.mk


#########################################################################
#########################################################################
#########################################################################

clean:
	$(MAKE) -C spl $@
	$(MAKE) -C boot $@
	rm -f $(ALL)
	rm -rf ${PWD}/spl/common/jz_serial.c
	rm -rf ${PWD}/spl/common/cpu.c
	rm -rf ${PWD}/spl/common/debug.c
	rm -rf ${PWD}/spl/common/common.c
	rm -rf $(PWD)/include/config.mk

########################################################################
