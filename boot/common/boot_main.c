#include <config.h>
#include <serial.h>
#include <boot_msg.h>
#include <boot.h>
#include <libc.h>
#include <debug.h>
#include <common.h>
#include <pm.h>

extern void print_logo(void);
extern void nand_boot(int boot_select);
extern void msc_boot(int msc_boot_select);
extern int update_firmware(int fw_select);
extern int board_private_init(void);
extern int usb_boot(unsigned zero, unsigned type, unsigned tags);


/*
 * Bootloader message (stored in MISC partition)
 */
struct bootloader_message g_boot_msg;

/*
 * Handle the command that specified in bootloader_message and return boot select.
 * Command:	"update firmware";
 * "boot into recovery";
 * any other commands we have already defined.
 * Ret:	1	Normal boot
 *		2	Recovery boot
 */
static int handle_bootloader_command(void)
{
	/* Command: boot-recovery */
	if ( !memcmp(g_boot_msg.command, "boot-recovery", strlen("boot-recovery")) ) {
		serial_puts_info("In handle_bootloader_command ... boot-recovery ...\n");
		return RECOVERY_BOOT;
	}

	/* Command: update-radio */
	if ( !memcmp(g_boot_msg.command, "update-radio", strlen("update-radio")) ) {
		serial_puts_info("In handle_bootloader_command ... update-radio ...\n");
		update_firmware(UPDATE_RADIO);
		return RECOVERY_BOOT;
	}

	/* Command: update-xboot */
	if ( !memcmp(g_boot_msg.command, "update-xboot", strlen("update-xboot")) ) {
		serial_puts_info("In handle_bootloader_command ... update-xboot ...\n");
		update_firmware(UPDATE_XBOOT);
		return RECOVERY_BOOT;
	}

	if (g_boot_msg.command[0] != '\0') {
		serial_puts_info("WARNING: bootloader_message -> command [0] is not '\\0' !\n");
	}

	serial_puts_info("In handle_bootloader_command ... default ...\n");

	//dump_ram(&g_boot_msg, sizeof(struct bootloader_message));

	return NORMAL_BOOT;
}

/*
 * Get USB boot keys states.
 * Ret:	Non-0	USB boot keys pressed
 *	0	not ...
 */
#if defined(FAST_BOOT_SUPPORT)
static inline int is_fast_boot_keys_pressed(void)
{
	__fast_keys_init();
	if(__fast_keys_presed()){
		return 1;
	}
	return 0;
}
#endif

/*
 * Get Recovery keys states.
 * Ret:	Non-0	Recovery keys pressed
 *	0	not ...
 */  
static inline int is_recovery_keys_pressed(void)
{
	__recovery_keys_init();
	if(__recovery_keys_presed()){
		return 1;
	}
	return 0;
}


#if 0
/*
 * Get if is run_in_test mode.
 * return true if run_in_test mode is on. 
 * (that means both DOCK_INT and DOCK_SPK pins are connected.)
 */
static inline int is_run_in_test(void)
{
	int i;

	__gpio_disable_pull(XBOOT_SEL_PRETEST_KEY1);
	__gpio_disable_pull(XBOOT_SEL_PRETEST_KEY2);
	__gpio_as_output(XBOOT_SEL_PRETEST_KEY1);	
	__gpio_as_input(XBOOT_SEL_PRETEST_KEY2);	

	for(i=2; i>0; i--) {
		__gpio_set_pin(XBOOT_SEL_PRETEST_KEY1);
		mdelay(1);
		if(!(__gpio_get_pin(XBOOT_SEL_PRETEST_KEY2)))
			break;
		mdelay(1);

		__gpio_clear_pin(XBOOT_SEL_PRETEST_KEY1);
		mdelay(1);
		if(__gpio_get_pin(XBOOT_SEL_PRETEST_KEY2))
			break;
		mdelay(1);
	}

	if(i==0)
		return 1;
	else 
		return 0;
}
#endif
/*
 * Get recovery signature and reset it.
 */
static int get_recovery_signature(void)
{
	unsigned int flag = cpm_get_scrpad();
	if ((flag & 0xffff) == RECOVERY_SIGNATURE) {
		/* clear the signature ,reset the signature to force into normal boot after factory reset*/
		cpm_set_scrpad(flag & ~(0xffff));
		return 1;
	} else {
		return 0;
	}
}
/*
 * Get boot keys, NAND boot or USB boot.
 * ret:	0	USB boot
 *	1 NAND boot, normal
 *	2 NAND boot, recovery
 *  3 PRETEST boot, pretest
 */
static int get_boot_keys(void)
{
#if defined(FAST_BOOT_SUPPORT)
	/* FAST boot keys */
	if (is_fast_boot_keys_pressed()){
		serial_puts_info("fast boot ...\n");
		return FAST_BOOT;
		}
#endif

	/* Recovery boot keys */
	if (is_recovery_keys_pressed()){
		serial_puts_info("recovery boot ...\n");
		return RECOVERY_BOOT;
	}
	/* Recovery signature */
	if(get_recovery_signature()){
		return RECOVERY_BOOT;
	}
	return NORMAL_BOOT;
}
/*
 * C entry of X-Boot.
 * Select boot routine
 */
void boot_main(void)
{
	int boot_select = 0;
	
	board_private_init();

#if defined(CONFIG_XBOOT_LOGO_FILE)
	print_logo();
	low_battery_judgement();
#endif
	/* First, handle boot keys. */
	serial_puts_info("Getting boot sel keys ...\n");
	boot_select = get_boot_keys();
	switch (boot_select) {
	case FAST_BOOT:
		serial_puts_info("Enter Fast boot routine ...\n");
		usb_boot(0, 0, 0);
		return;
	case RECOVERY_BOOT:
#if defined(CONFIG_NAND_X_BOOT)
		serial_puts_info("Enter Recovery(NAND boot) routine ...\n");
		nand_boot(RECOVERY_BOOT);
#elif defined(CONFIG_MSC_X_BOOT)
		serial_puts_info("Enter Recovery(MSC boot) routine ...\n");
		msc_boot(RECOVERY_BOOT);//do not modify here
#endif
		return;
	case PRETEST_BOOT:
		serial_puts_info("Enter Pretest boot routine ...\n");
#if defined(CONFIG_NAND_X_BOOT)
		nand_boot(PRETEST_BOOT);
#elif defined(CONFIG_MSC_X_BOOT)
		msc_boot(PRETEST_BOOT);
#endif
		return;
	};
	/* Second, handle boot message (MISC partition). */
	serial_puts_info("Getting boot message ...\n");
#if defined(CONFIG_NAND_X_BOOT)
	memset(&g_boot_msg, '\0', sizeof(struct bootloader_message));
	if ( get_bootloader_message(&g_boot_msg) ) {
		serial_puts_info("Got bootloader message failed !\n");
		nand_boot(NORMAL_BOOT);
		return;

	} else {
		if (handle_bootloader_command() == RECOVERY_BOOT) {
			serial_puts_info("Bootloader message: boot-recovery\n");
			nand_boot(RECOVERY_BOOT);
			return;

		} else {
			nand_boot(NORMAL_BOOT);
			return;
		}
	}
#elif defined(CONFIG_MSC_X_BOOT)
	msc_boot(NORMAL_BOOT);
#endif
}
