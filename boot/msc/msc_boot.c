/*
 * Copyright (C) 2007 Ingenic Semiconductor Inc.
 * Copyright (C) 2010 Ingenic Semiconductor Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <config.h>
#include <serial.h>
#include <boot_img.h>
#include <boot.h>
#include <libc.h>
#include <debug.h>
#include <image.h>
#include "jz4760_android_lcd.h"

extern void flush_cache_all(void);
extern int init_boot_linux(unsigned char* data_buf, unsigned int data_size);
extern int do_msc(unsigned long addr, unsigned long off, unsigned long size);
/*
 * Msc boot routine
 */
void flush_dcache_all(void);
void msc_boot(int msc_boot_select)
{
	unsigned int offset, size;
	void (*kernel)(int, char **, char *);
	int isfast = 0;
	//unsigned int crc;
	//int i;
    //static u32 *param_addr = 0;
    //static u8 *tmpbuf = 0;
        static u8 cmdline[256] = CFG_CMDLINE;

	serial_puts_info("Enter msc_boot routine ...\n");
	
	switch (msc_boot_select) {
	case NORMAL_BOOT:
		isfast = 1;
		offset = CFG_BOOT_OFFS;
		size = CFG_BOOT_SIZE;
		
#ifdef BOOTARGS_NORMAL
		strcpy((char *)cmdline, BOOTARGS_NORMAL);
#endif
		
		serial_puts_info("Normal boot ...\n");
		break;
	case RECOVERY_BOOT:
		isfast = 1;
		offset = CFG_RECOVERY_OFFS;
		size = CFG_RECOVERY_SIZE;
		
#ifdef BOOTARGS_RECOVERY
		strcpy((char *)cmdline, BOOTARGS_RECOVERY);
#endif
		
		serial_puts_info("Recovery boot ...\n");
		break;
#if defined(CONFIG_JZ4760_PT701_8)
	case PRETEST_BOOT:	      
		offset = CFG_PRETEST_OFFS;
		size = CFG_PRETEST_SIZE;
		serial_puts_info("Pretest boot ...\n");
		break;
#endif
	default:
		serial_puts_info("Get msc boot select failed, defualt normal boot ...\n");
		offset = CFG_BOOT_OFFS;
		size = CFG_BOOT_SIZE;
		break;
	}
	serial_put_hex(offset);
	serial_put_hex(size);
	serial_puts_info("Load kernel from MSC ...\n");
	/* Load kernel and ramdisk */
	if(isfast)
	{
#if 0
		/* use timer to profile the time
		 * timer run at 12MHz
		 * time = counter_delta / 12000000
		 */
		(*(volatile unsigned short *)0xb00020ec) = (1<<15) | (2<<1);
		(*(volatile unsigned short *)0xb0002014) = (1<<15) ;
		unsigned int cnt1 = *((volatile unsigned long *)0xb00020e8);
#endif

		do_msc(CFG_KERNEL_DST, offset, 2048);
		size = calculate_size((u8 *)CFG_KERNEL_DST,2048);
		do_msc(CFG_KERNEL_DST+2048, offset+2048, size);
#if 0
		unsigned int cnt2 = *((volatile unsigned long *)0xb00020e8);
		serial_puts("\n\n");
		serial_put_hex(cnt2 -cnt1);
		serial_puts("\n\n");
#endif
	}else
		do_msc(CFG_KERNEL_DST, offset, size);

	serial_puts_info("Prepare kernel parameters ...\n");
	/* init kernel, ramdisk and prepare parameters */
	if (init_boot_linux((unsigned char*)CFG_KERNEL_DST, size) == 0) {
		serial_puts_info("Jump to kernel start Addr 0x");
		dump_uint(CFG_KERNEL_DST);
		serial_puts("\n\n");
#if CONFIG_XBOOT_LOGO_FILE
//		__lcd_display_off();
#endif
		kernel = (void (*)(int, char **, char *))CFG_KERNEL_DST;
		flush_cache_all();
		/* Jump to kernel image */
		(*kernel)(2, (char **)(PARAM_BASE + 16), (char *)PARAM_BASE);
		serial_puts_info("We should not come here ... \n");
	} else
		serial_puts_info("MSC boot error...\n");
	while(1);
}
