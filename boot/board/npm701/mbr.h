#ifndef MBR_____________INCLUDE
#define MBR_____________INCLUDE

#define KBYTE  (1024LL)
#define MBYTE  ((KBYTE)*(KBYTE))

#define LINUX_FS 0x83
#define FAT_FS 0x0b

#define MBR_P1_OFFSET (8*MBYTE)
#define MBR_P1_SIZE (32*MBYTE)
#define MBR_P1_TYPE LINUX_FS

#define MBR_P2_OFFSET (40*MBYTE)
#define MBR_P2_SIZE (8150*MBYTE)
#define MBR_P2_TYPE LINUX_FS

#endif /* MBR_____________INCLUDE */
