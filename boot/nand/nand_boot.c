/*
 * Copyright (C) 2007 Ingenic Semiconductor Inc.
 * Copyright (C) 2010 Ingenic Semiconductor Inc.
 * Author: Jason <xwang@ingenic.cn>
 * Author: Aaron <hfwang@ingenic.cn>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <config.h>
#include <nand.h>
#include <serial.h>
#include <boot_img.h>
#include <boot.h>
#include <libc.h>
#include <debug.h>
#include <lcd.h>

extern void init_cpu(void);
//extern void flush_cache_all(void);
extern int init_boot_linux(unsigned char* data_buf, unsigned int data_size);

/*
 * Nand boot routine
 */

extern unsigned long CRC_32( unsigned char * aData, unsigned long aSize );
void nand_boot(int nand_boot_select)
{
	unsigned int offset, size;
	void (*kernel)(int, char **, char *);

	int i;
	static u32 *param_addr = 0;
        static u8 *tmpbuf = 0;
        static u8 cmdline[256] = CFG_CMDLINE;

	serial_puts_info("Enter nand_boot routine ...\n");

	switch (nand_boot_select) {
	case NORMAL_BOOT:
		offset = CFG_BOOT_OFFS;
		size = CFG_BOOT_SIZE;
#ifdef BOOTARGS_NORMAL
		strcpy((char *)cmdline, BOOTARGS_NORMAL);
#endif
		serial_puts_info("Normal boot ...\n");
		break;
	case RECOVERY_BOOT:
		offset = CFG_RECOVERY_OFFS;
		size = CFG_RECOVERY_SIZE;
#ifdef BOOTARGS_RECOVERY
		strcpy((char *)cmdline, BOOTARGS_RECOVERY);
#endif
		serial_puts_info("Recovery boot ...\n");
		break;
#if defined(CONFIG_JZ4760_PT701_8)
	case PRETEST_BOOT:	      
		offset = CFG_PRETEST_OFFS;
		size = CFG_PRETEST_SIZE;
		serial_puts_info("Pretest boot ...\n");
		break;
#endif
	default:
		serial_puts_info("Get nand boot select failed, defualt normal boot ...\n");
		offset = CFG_BOOT_OFFS;
		size = CFG_BOOT_SIZE;
		break;
	}

	serial_puts_info("Load kernel from NAND ...\nCRC32:");
	/* Load kernel and ramdisk */
	nand_load(offset, size, (u8 *)CFG_KERNEL_DST);
#if  0
	serial_put_hex(CRC_32(CFG_KERNEL_DST,2973696));
	serial_put_hex(*((unsigned int *)(CFG_KERNEL_DST+0)));
	serial_put_hex(*((unsigned int *)(CFG_KERNEL_DST+4)));
	serial_put_hex(*((unsigned int *)(CFG_KERNEL_DST+8)));
	serial_put_hex(*((unsigned int *)(CFG_KERNEL_DST+12)));
#endif

	serial_puts_info("Prepare kernel parameters ...\n");
	/* init kernel, ramdisk and prepare parameters */
	if (init_boot_linux((unsigned char*)CFG_KERNEL_DST, size) == 0) {
		serial_puts_info("Jump to kernel start Addr 0x");
		dump_uint(CFG_KERNEL_DST);
		serial_puts("\n\n");
		kernel = (void (*)(int, char **, char *))CFG_KERNEL_DST;
		flush_cache_all();
#if CONFIG_XBOOT_LOGO_FILE
		__lcd_display_off();
#endif
		/* Jump to kernel image */
		(*kernel)(2, (char **)(PARAM_BASE + 16), (char *)PARAM_BASE);
		serial_puts_info("We should not come here ... \n");
	} else
		serial_puts_info("Magic number error,boot error...\n");
}
