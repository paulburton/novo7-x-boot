/* Copyright 2007, Google Inc. */

#include "boot_tmp.h"
#include "flash.h"
#include <config.h>
#include <serial.h>
#include <debug.h>
#include <common.h>

#define MTDPART_SIZ_FULL        (0)

#ifdef CONFIG_JZ4750
/* Reserve 32MB for bootloader, splash1, splash2 and radiofw */
    #define MISC_OFFSET		(32  * 0x100000)
    #define MISC_SIZE		(  1 * 0x100000)
    #define RECOVERY_SIZE	(  5 * 0x100000)
    #define BOOT_SIZE		(  4 * 0x100000)
    #define SYSTEM_SIZE		(100 * 0x100000)
    #define USERDATA_SIZE	(1   * 0x100000)
    #define CACHE_SIZE		(100 * 0x100000)
    #define KPANIC_SIZE     ( 2 * 0x100000)
    #define STORAGE_SIZE	(MTDPART_SIZ_FULL)
#else
/* Reserve 32MB for bootloader, splash1, splash2 and radiofw */
#if defined(CONFIG_JZ4760_Z800)
    #define MISC_OFFSET             (32  * 0x100000)
    #define MISC_SIZE               (  1 * 0x100000)
    #define RECOVERY_SIZE           (  5 * 0x100000)
    #define BOOT_SIZE               (  4 * 0x100000)
    #define SYSTEM_SIZE             ((90 + 45) * 0x100000)
    #define USERDATA_SIZE           ((90 - 45 + 128) * 0x100000)
    #define CACHE_SIZE              (32 * 0x100000)
    #define KPANIC_SIZE             ( 2 * 0x100000)
    #define STORAGE_SIZE            (MTDPART_SIZ_FULL)

#elif (defined(CONFIG_JZ4760_PT701) || defined(CONFIG_JZ4760_PT701_8) || defined(CONFIG_JZ4760_TABLET_P2) || \
       defined(CONFIG_JZ4760_TABLET_8II)||defined(CONFIG_JZ4760_PX7_2R))
    #define MISC_OFFSET             (32  * 0x100000)
    #define MISC_SIZE               (  4 * 0x100000)
    #define RECOVERY_SIZE           (  12 * 0x100000)
    #define BOOT_SIZE               (  12 * 0x100000)
    #define SYSTEM_SIZE             (256 * 0x100000)
    #define USERDATA_SIZE           (500 * 0x100000)
    #define CACHE_SIZE              (128 * 0x100000)
    #define KPANIC_SIZE             ( 4 * 0x100000)
    #define STORAGE_SIZE        (MTDPART_SIZ_FULL)

#elif defined(CONFIG_JZ4760_ALTAIR)
    #define MISC_OFFSET             (32  * 0x100000)
    #define MISC_SIZE               (  1 * 0x100000)
    #define RECOVERY_SIZE           (  5 * 0x100000)
    #define BOOT_SIZE               (  4 * 0x100000)
    #define SYSTEM_SIZE             ((90 + 45) * 0x100000)
    #define USERDATA_SIZE           ((90 - 45) * 0x100000)
    #define CACHE_SIZE              (32 * 0x100000)
    #define KPANIC_SIZE             ( 2 * 0x100000)
    #define STORAGE_SIZE            (MTDPART_SIZ_FULL)


#elif defined(CONFIG_JZ4760_CRATER) || defined(CONFIG_JZ4760_LYNX) || defined(CONFIG_JZ4770_MAPLE) || defined(CONFIG_JZ4770_NPM701) || defined(CONFIG_JZ4770_O1)||defined(CONFIG_JZ4770_UMIDO706)

    #define MISC_OFFSET            (32  * 0x100000)
    #define MISC_SIZE                  (  1 * 0x100000)
    #define RECOVERY_SIZE        (  5 * 0x100000)
    #define BOOT_SIZE                 (  4 * 0x100000)
    #define SYSTEM_SIZE             ( 128 * 0x100000)
    #define USERDATA_SIZE        (  256 * 0x100000)
    #define CACHE_SIZE               ( 32 * 0x100000)
    #define KPANIC_SIZE             (   2 * 0x100000)
    #define STORAGE_SIZE           (MTDPART_SIZ_FULL)

#elif defined(CONFIG_JZ4760_LEPUS)
    #define MISC_OFFSET            (32  * 0x100000)
    #define MISC_SIZE                  (  1 * 0x100000)
    #define RECOVERY_SIZE        (  5 * 0x100000)
    #define BOOT_SIZE                 (  4 * 0x100000)
    #define SYSTEM_SIZE             ( 128 * 0x100000)
    #define USERDATA_SIZE        (  48 * 0x100000)
    #define CACHE_SIZE               ( 32 * 0x100000)
    #define KPANIC_SIZE             (   2 * 0x100000)
    #define STORAGE_SIZE           (MTDPART_SIZ_FULL)

#else
    #define MISC_OFFSET            (32  * 0x100000)
    #define MISC_SIZE                  (  1 * 0x100000)
    #define RECOVERY_SIZE        (  5 * 0x100000)
    #define BOOT_SIZE                 (  4 * 0x100000)
    #define SYSTEM_SIZE             ( 128 * 0x100000)
    #define USERDATA_SIZE        (  48 * 0x100000)
    #define CACHE_SIZE               ( 32 * 0x100000)
    #define KPANIC_SIZE             (   2 * 0x100000)
    #define STORAGE_SIZE           (MTDPART_SIZ_FULL)

#endif
#endif

ptentry PTABLE[] = {
    {
        .start = MISC_OFFSET,
        .length = MISC_SIZE,
        .name = "misc",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE),
        .length = RECOVERY_SIZE,
        .name = "recovery",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE),
        .length = BOOT_SIZE,
        .name = "boot",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE+BOOT_SIZE),
        .length = SYSTEM_SIZE,
        .name = "system",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE+BOOT_SIZE+SYSTEM_SIZE),
        .length = USERDATA_SIZE,
        .name = "userdata",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE+BOOT_SIZE+SYSTEM_SIZE+USERDATA_SIZE),
        .length = CACHE_SIZE,
        .name = "cache",
    },
    {
	    .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE+BOOT_SIZE+SYSTEM_SIZE+USERDATA_SIZE+CACHE_SIZE),
	    .length = KPANIC_SIZE,
	    .name = "kpanic",
    },
    {
        .start = (MISC_OFFSET+MISC_SIZE+RECOVERY_SIZE+BOOT_SIZE+SYSTEM_SIZE+USERDATA_SIZE+CACHE_SIZE+KPANIC_SIZE),
        .length = STORAGE_SIZE,
        .name = "storage",
    },
    {
        .name = "",
    },
};

const char *board_cmdline(void) 
{
    return "mem=112M androidboot.console=ttyMSM0 console=ttyMSM0";
}

unsigned board_machtype(void)
{
    return 1439;
}

void board_init(void)
{
	unsigned n;

	flash_init();
	/* if we already have partitions from elsewhere,
	** don't use the hardcoded ones
	*/
	if(flash_get_ptn_count() == 0) {
		for(n = 0; n < sizeof(PTABLE)/sizeof(PTABLE[0]); n++) {
			flash_add_ptn(PTABLE + n);
		}
	}
}

void board_usb_init(void)
{
}

void board_ulpi_init(void)
{
}

void board_reboot(void)
{
	REG_WDT_TCSR = WDT_TCSR_PRESCALE4 | WDT_TCSR_EXT_EN;
	REG_WDT_TCNT = 0;
	REG_WDT_TDR = JZ_EXTAL/1000;   /* reset after 4ms */
	REG_TCU_TSCR = 1 << 16;//TCU_TSCR_WDTSC; /* enable wdt clock */
	REG_WDT_TCER = WDT_TCER_TCEN;  /* wdt start */
	while (1);
}

void board_getvar(const char *name, char *value)
{
}

unsigned long initdram(int board_type)
{
        unsigned long size=0;

        u32 rows, cols, dw, banks;

#ifdef CONFIG_JZ4750
	u32 dmcr;

        dmcr = REG_EMC_DMCR;
        rows = 11 + ((dmcr & EMC_DMCR_RA_MASK) >> EMC_DMCR_RA_BIT);
        cols = 8 + ((dmcr & EMC_DMCR_CA_MASK) >> EMC_DMCR_CA_BIT);
        dw = (dmcr & EMC_DMCR_BW) ? 2 : 4;
        banks = (dmcr & EMC_DMCR_BA) ? 4 : 2;

        size = (1 << (rows + cols)) * dw * banks;
        size *= CONFIG_NR_DRAM_BANKS;

#elif defined(CONFIG_JZ4760) || defined(CONFIG_JZ4760B) || defined(CONFIG_JZ4770)
	u32 ddr_cfg;
        ddr_cfg = REG_DDRC_CFG;
        rows = 12 + ((ddr_cfg & DDRC_CFG_ROW_MASK) >> DDRC_CFG_ROW_BIT);
        cols = 8 + ((ddr_cfg & DDRC_CFG_COL_MASK) >> DDRC_CFG_COL_BIT);

        dw = (ddr_cfg & DDRC_CFG_DW) ? 4 : 2;
        banks = (ddr_cfg & DDRC_CFG_BA) ? 8 : 4;

        size = (1 << (rows + cols)) * dw * banks;
        size *= (DDR_CS1EN + DDR_CS0EN);

#endif	/*CONFIG_JZ4760*/

        return size;

}
